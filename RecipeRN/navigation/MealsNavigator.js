import React from 'react'
import { Platform, Text } from 'react-native'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createDrawerNavigator} from 'react-navigation-drawer'
import {Ionicons} from '@expo/vector-icons'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'

import CategorisScreen from '../screens/CategoriesScreen'
import CategorieSMealsScreen from '../screens/CategoryMealsScreen';
import MealDetailScreen from '../screens/MealDetailScreen'
import Colors from '../constants/Colors';
import FavouritesScreen from '../screens/FavoritesScreen'
import FiltersScreen from '../screens/FiltersScreen'


const defaultStackNavOptions = {
        headerStyle: {
            backgroundColor: Platform.OS === 'android' ? Colors.primaryColor : ''
        },
        headerTitleStyle: {
            fontFamily: 'open-sans-bold'
        },
        headerBackTitleStyle: {
            fontFamily: 'open-sans'
        },
        headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primaryColor
    
}

const MealNavigator = createStackNavigator({
    Categories: {screen: CategorisScreen},
    CategoryMeals: {
        screen: CategorieSMealsScreen
    },
    MealDetail: MealDetailScreen
},
{
    defaultNavigationOptions: defaultStackNavOptions
}
);

const FavNavigator = createStackNavigator({
    Favoutires: FavouritesScreen,
    MealDetail: MealDetailScreen
}, {
    defaultNavigationOptions: defaultStackNavOptions
})

const tabScreenConfig = {
        Meals: { screen: MealNavigator, navigationOptions: {
          tabBarIcon: (tabInfo) => {
              return <Ionicons name="ios-restaurant" size={25} color={tabInfo.tintColor}/>
          },
          tabBarColor: Colors.primaryColor,
          tabBarLabel: Platform.OS === 'android' ? <Text style={{fontFamily: 'open-sans-bold'}}>Meals</Text> : 'Meals'  
        }},
        Favourites: {screen: FavNavigator, navigationOptions: {
           tabBarIcon: (tabInfo) => {
               return <Ionicons name="ios-star" size={25} color={tabInfo.tintColor}/>
           },
           tabBarColor: Colors.accentColor,
           tabBarLabel: Platform.OS === 'android' ? <Text style={{fontFamily: 'open-sans-bold'}}>Favourites</Text> : 'Favourites'  
        }}
   }


const MealsFavTabNavigator = Platform.OS === 'android' ? createMaterialBottomTabNavigator(tabScreenConfig, {
    activeColor: 'white',
    shifting: true
}) 
: createBottomTabNavigator(tabScreenConfig, {
    tabBarOptions: {
       labelStyle: {
           fontFamily: 'open-sans-bold'
       }, 
       activeTintColor: Colors.accentColor, 
    }
});

const FiltersNavigator = createStackNavigator({
    Filters: FiltersScreen
},  {
    // navigationOptions:{
    //     drawerLabel: 'Filters!!'
    // },
    defaultNavigationOptions: defaultStackNavOptions
})


const MainNavigation = createDrawerNavigator({
    MealsFavs:  {screen : MealsFavTabNavigator,
    navigationOptions: {
        drawerLabel: 'Meals'
    }},
    Filters: {screen: FiltersNavigator , navigationOptions: {
        drawerLabel: 'Filters'
    }}
}, {
    contentOptions: {
        activeTintColor: Colors.accentColor,
        labelStyle: {
            fontFamily: 'open-sans-bold'
        }
    }
})



export default createAppContainer(MainNavigation)