import React from 'react';
import {View, Text, StyleSheet} from 'react-native'
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import MealList from '../components/MealList'
import {useSelector} from 'react-redux'

import HeaderButton from '../components/HeadreButton'
import DefaultText from '../components/DefaultText'

const FavouritesScreen = props => {

    const availableMeals = useSelector(state => state.meals.favouriteMeals)

    if(availableMeals.length === 0 || !availableMeals){
        return <View style={styles.noFav}><DefaultText>No favourite meals found. Start adding some</DefaultText></View>
    }

    return (
    <MealList listData={availableMeals} navigation={props.navigation}/>
);
}

FavouritesScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Your Favourites!',
        headerLeft: (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item title="menu" iconName="ios-menu" onPress={() => {
                    navData.navigation.toggleDrawer();
                }}/>
            </HeaderButtons>
        )
    }
}


const styles = StyleSheet.create({
    noFav: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})



export default FavouritesScreen