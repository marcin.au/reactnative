import React , {useEffect, useCallback} from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {ScrollView, View, Image, Text, StyleSheet, Button} from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons'
import HeaderButton from '../components/HeadreButton'
import DefaultText from '../components/DefaultText'
import {toggleFavourite} from '../store/actions/meals'

const ListItem = props => {
    return <View style={styles.listItem}>
        <DefaultText>{props.children}</DefaultText>
    </View>
}

const MealDetailScreen = props => {

    const allMeals = useSelector(state => state.meals.meals)
    

    const mealId = props.navigation.getParam('mealId')
    const currentMealsIsFavourite = useSelector(state => state.meals.favouriteMeals.some(meal => meal.id === mealId))
    const displayDetail = allMeals.find(meal => meal.id === mealId)

    const dispatch = useDispatch();
    const toggleFavHandler = useCallback(() => {
        dispatch(toggleFavourite(mealId))
    },[dispatch, mealId])

    useEffect(() => {
        props.navigation.setParams({ toggleFav : toggleFavHandler})
    }, [toggleFavHandler])

    useEffect(() =>{
        props.navigation.setParams({
            isFav: currentMealsIsFavourite
        })
    }, [currentMealsIsFavourite])

    return (
    <ScrollView>
        <Image source={{uri: displayDetail.imageUrl}} style={styles.image}/>
        <View style={styles.details}>
                    <DefaultText>{displayDetail.duration}m</DefaultText>
                    <DefaultText>{displayDetail.complexity.toUpperCase()}</DefaultText>
                    <DefaultText>{displayDetail.affordability.toUpperCase()}</DefaultText>
        </View>
        <Text style={styles.title}>Ingredients</Text>
        {displayDetail.ingredients.map(ingredient => 
            <ListItem key={ingredient}>{ingredient}</ListItem>
         )}
        <Text style={styles.title}>Steps</Text>
        {displayDetail.steps.map(step => <Text key={step}>{step}</Text>)}
    </ScrollView>
);
}

MealDetailScreen.navigationOptions = navigationData => {

    //const mealId = navigationData.navigation.getParam('mealId')
    const mealTitle = navigationData.navigation.getParam('mealTitle')
    const toggleFavourite = navigationData.navigation.getParam('toggleFav')
    //const displayDetail = MEALS.find(meal => meal.id === mealId)
    const isFav = navigationData.navigation.getParam('isFav')

    return {
        headerTitle: mealTitle,
        headerRight: <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item title='Favourite' iconName={isFav ? 'ios-star': 'ios-star-outline'} onPress={toggleFavourite}/>
        </HeaderButtons>
    }
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 200
    },
    details: {
        flexDirection: 'row',
        padding: 15,
        justifyContent: 'space-between'
    },
    title: {
        fontFamily: 'open-sans-bold',
        fontSize: 22,
        textAlign: 'center'

    },
    listItem:{
        marginVertical: 10,
        marginHorizontal: 20,
        borderColor: '#ccc',
        borderWidth: 1,
        padding: 10
    }
});

export default MealDetailScreen